import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import lejos.hardware.BrickFinder;
import lejos.hardware.Button;
import lejos.hardware.Sound;
import lejos.hardware.lcd.Font;
import lejos.hardware.lcd.GraphicsLCD;

public class Task1
{
	static GraphicsLCD g = BrickFinder.getDefault().getGraphicsLCD();
	static int win = 0;
	static int fail = 0;
	static int key = 0;
	static int stage = 0;
	static String[] msg = { "Quess number between 1 and 4", "Try again? Enter: Yes Esc: No" };
	static ArrayList<String> mem = new ArrayList<>();
	static HashMap<Integer, String> keys = new HashMap<>();
	static HashMap<String, String> btn = new HashMap<>();

	public static void main(String[] args)
	{
		keys.put(1, "UP");
		keys.put(8, "RIGHT");
		keys.put(4, "DOWN");
		keys.put(16, "LEFT");
		btn.put("up", "UP is wrong");
		btn.put("right", "RIGHT is wrong");
		btn.put("down", "DOWN is wrong");
		btn.put("left", "LEFT is wrong");
		btn.put("good", "is correct!");
		btn.put("bad", "Incorrect button");
		Random rng = new Random();
		int[] keys = { 1, 4, 8, 16 };
		int rand = rng.nextInt(4);
		Sound.setVolume(2);
		Sound.beepSequenceUp();
		g.setFont(Font.getSmallFont());
		funky: while (true)
		{
			stage = 0;
			refDisp();
			refMsg();
			int in = Button.waitForAnyPress();
			stage = 1;
			if (in == keys[rand])
			{
				win++;
				key = in;
				rand = rng.nextInt(4);
				refDisp("good");
			}
			else
			{
				switch (in)
				{
					case 1:
						fail++;
						refDisp("up");
						break;
					case 4:
						fail++;
						refDisp("down");
						break;
					case 8:
						fail++;
						refDisp("right");
						break;
					case 16:
						fail++;
						refDisp("left");
						break;
					default:
						refDisp("bad");
						break;
				}
			}
			while (true)
			{
				in = Button.waitForAnyPress();
				if (in == 32)
				{
					break funky;
				}
				else if (in == 2)
				{
					break;
				}
			}
		}
		Sound.beepSequence();
	}

	public static void refDisp()
	{
		float rate = (win > 0 || fail > 0) ? (((float) win / (win + fail)) * 100) : 0;
		g.clear();
		g.drawString(String.format("UP: 1 RIGHT: 2 DOWN: 3 LEFT: 4"), 0, 0, 0);
		g.drawString(String.format("Win:%3d Fail:%3d Ratio:%3.0f%%", win, fail, rate), 0, 10, 0);
		g.drawLine(0, 18, g.getWidth(), 18);
		g.drawString(String.format("%s", msg[stage]), 0, 20, 0);
		g.drawLine(0, 28, g.getWidth(), 28);
	}

	public static void refDisp(String button)
	{
		refDisp();
		if ((button.equals("good")))
		{
			mem.add(String.format("%s %s", keys.get(key), btn.get(button)));
		}
		else
		{
			mem.add(btn.get(button));
		}
		if (mem.size() > 10)
		{
			mem.remove(0);
		}
		refMsg();
	}

	public static void refMsg()
	{
		int y = 30;
		for (String string : mem)
		{
			g.drawString(string, 0, y, 0);
			y += 10;
		}
	}
}
