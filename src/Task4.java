import lejos.hardware.BrickFinder;
import lejos.hardware.Button;
import lejos.hardware.Sound;
import lejos.hardware.lcd.Font;
import lejos.hardware.lcd.GraphicsLCD;
import lejos.hardware.motor.EV3LargeRegulatedMotor;
import lejos.hardware.port.MotorPort;
import lejos.hardware.port.SensorPort;
import lejos.hardware.sensor.EV3IRSensor;
import lejos.robotics.RegulatedMotor;
import lejos.utility.Delay;

public class Task4
{
	static irThread irt = new irThread(new EV3IRSensor(SensorPort.S1));
	static GraphicsLCD g = BrickFinder.getDefault().getGraphicsLCD();
	static int cmd = 0;

	public static void main(String[] args)
	{
		RegulatedMotor left = new EV3LargeRegulatedMotor(MotorPort.B);
		RegulatedMotor right = new EV3LargeRegulatedMotor(MotorPort.C);
		irt.start();
		dispThread dt = new dispThread();
		dt.start();
		Sound.setVolume(2);
		Sound.beepSequenceUp();
		g.setFont(Font.getSmallFont());
		loopy: while (Button.ESCAPE.isUp())
		{
			switch (cmd)
			{
				case 1: // TOP-LEFT
					left.forward();
					right.forward();
					break;
				case 2: // BOTTOM-LEFT
					left.backward();
					right.forward();
					break;
				case 3: // TOP-RIGHT
					left.backward();
					right.backward();
					break;
				case 4: // BOTTOM-RIGHT
					left.forward();
					right.backward();
					break;
				case 5: // TOP-LEFT + TOP-RIGHT
					break;
				case 6: // TOP-LEFT + BOTTOM-RIGHT
					break;
				case 7: // BOTTOM-LEFT + TOP-RIGHT
					break;
				case 8: // BOTTOM-LEFT + BOTTOM-RIGHT
					break;
				case 9: // CENTRE/BEACON
					irt.quit();
					break loopy;
				case 10: // BOTTOM-LEFT + TOP-LEFT
					break;
				case 11: // TOP-RIGHT + BOTTOM-RIGHT
					break;
				default:
					left.stop(true);
					right.stop(true);
					break;
			}
		}
		dt.quit();
		left.close();
		right.close();
		Sound.beepSequence();
	}
}

class dispThread extends Thread
{
	private GraphicsLCD g = Task4.g;
	private Thread thisThread = Thread.currentThread();

	@Override
	public void run()
	{
		while (thisThread != null)
		{
			g.clear();
			g.drawString("Forward", 0, 0, 0);
			g.drawString("Backward", g.getWidth(), 0, GraphicsLCD.RIGHT);
			g.drawString("Quit", g.getWidth() / 2, 0, GraphicsLCD.HCENTER);
			g.drawString("Turn Left", 0, g.getHeight(), GraphicsLCD.BOTTOM);
			g.drawString("Turn Right", g.getWidth(), g.getHeight(), GraphicsLCD.BOTTOM | GraphicsLCD.RIGHT);
			g.drawString(String.format("IR is %s", (Task4.irt.isAlive()) ? "ONLINE" : "OFFLINE"), g.getWidth() / 2,
					g.getHeight() / 2, GraphicsLCD.HCENTER);
			Delay.msDelay(80);
		}
	}

	public void quit()
	{
		thisThread = null;
	}
}

class irThread extends Thread
{
	private EV3IRSensor ir;
	private Thread thisThread = Thread.currentThread();

	public irThread(EV3IRSensor sensor)
	{
		ir = sensor;
	}

	@Override
	public void run()
	{
		while (thisThread != null)
		{
			try
			{
				Task4.cmd = ir.getRemoteCommand(0);
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
		ir.close();
	}

	public void quit()
	{
		thisThread = null;
	}
}
