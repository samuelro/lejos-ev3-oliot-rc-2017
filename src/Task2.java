import lejos.hardware.BrickFinder;
import lejos.hardware.Button;
import lejos.hardware.Sound;
import lejos.hardware.lcd.Font;
import lejos.hardware.lcd.GraphicsLCD;
import lejos.hardware.port.SensorPort;
import lejos.hardware.sensor.EV3TouchSensor;
import lejos.utility.Delay;

public class Task2
{
	private static final GraphicsLCD g = BrickFinder.getDefault().getGraphicsLCD();
	private static int touchCount = 0;

	public static void main(String[] args) throws InterruptedException
	{
		Sound.setVolume(2);
		Sound.beepSequenceUp();
		g.setFont(Font.getSmallFont());
		EV3TouchSensor touch = new EV3TouchSensor(SensorPort.S1);
		float[] sample = new float[touch.sampleSize()];
		boolean touchPress = false;
		loopy: while (true)
		{
			g.drawString("You can now start", 0, 0, 0);
			g.drawString("touching the Brick", 0, 10, 0);
			touch.fetchSample(sample, 0);
			if (Button.ESCAPE.isDown())
			{
				break loopy;
			}
			if (Button.readButtons() != Button.ID_ESCAPE && Button.readButtons() != 0)
			{
				g.drawString(String.format("Painalluksia: %d", touchCount), 0, (g.getHeight() / 2), 0);
				endy: while (true)
				{
					g.drawString("Try again?", 0, ((g.getHeight() / 2) + 10), 0);
					g.drawString("LEFT: Yes RIGHT: No", 0, ((g.getHeight() / 2) + 20), 0);
					int in = Button.waitForAnyPress();
					switch (in)
					{
						case Button.ID_LEFT:
							break endy;
						case Button.ID_RIGHT:
							break loopy;
					}
				}
				Sound.beep();
				g.clear();
				touchCount = 0;
				Delay.msDelay(500);
			}
			while (sample[0] == 1)
			{
				touch.fetchSample(sample, 0);
				touchPress = true;
			}
			if (touchPress)
			{
				touchPress = false;
				touchCount++;
			}
		}
		touch.close();
		Sound.beepSequence();
	}
}
