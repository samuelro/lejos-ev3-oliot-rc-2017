import java.util.Random;

import custom.Icons;
import lejos.hardware.BrickFinder;
import lejos.hardware.Button;
import lejos.hardware.Sound;
import lejos.hardware.lcd.Font;
import lejos.hardware.lcd.GraphicsLCD;
import lejos.hardware.port.SensorPort;
import lejos.hardware.sensor.EV3ColorSensor;
import lejos.robotics.Color;
import lejos.utility.Delay;

public class Task3
{
	static GraphicsLCD g = BrickFinder.getDefault().getGraphicsLCD();
	static int win = 0, lose = 0, tie = 0;
	static String[] msg = new String[] { "Exit", "Rock", "Paper", "Scissors" };
	static String[][] info = new String[][] {
			{ "exited", "exited", "exited", "exited" },
			{ "exited", "tied", "win", "lose" },
			{ "exited", "lose", "tied", "win" },
			{ "exited", "win", "lose", "tied" } };

	public static void main(String[] args)
	{
		Sound.setVolume(2);
		Sound.beepSequenceUp();
		g.setFont(Font.getSmallFont());
		EV3ColorSensor cs = new EV3ColorSensor(SensorPort.S1);
		Random rng = new Random();
		int rand = rng.nextInt(2) + 1;
		int plr = 0;
		loopy: while (!Button.ESCAPE.isDown())
		{
			plr = 0;
			dispInfo();
			Delay.msDelay(500);
			while (!Button.ESCAPE.isDown() && plr < 1)
			{
				switch (cs.getColorID())
				{
					case Color.RED:
						plr = 1;
						break;
					case Color.GREEN:
						plr = 2;
						break;
					case Color.BLUE:
						plr = 3;
						break;
				}
			}
			switch (info[rand][plr])
			{
				case "tied":
					Sound.twoBeeps();
					tie++;
					break;
				case "win":
					Sound.beepSequenceUp();
					win++;
					break;
				case "lose":
					Sound.buzz();
					lose++;
					break;
				case "exited":
					break loopy;
			}
			dispStats(plr, rand);
			Delay.msDelay(1000);
			staty: while (!Button.ESCAPE.isDown())
			{
				switch (cs.getColorID())
				{
					case Color.WHITE:
						plr = 0;
						rand = rng.nextInt(2) + 1;
						break staty;
					case Color.YELLOW:
						break loopy;
				}
			}
		}
		Sound.beepSequence();
	}

	private static void disp()
	{
		g.clear();
		Icons.escapeSmall(g.getWidth() - 33, 0, "quit");
		g.drawLine(0, 24, g.getWidth(), 24);
		g.drawString(String.format("Win: %2d Lose: %2d Tie: %2d", win, lose, tie), 0, 26, 0);
		g.drawLine(0, 34, g.getWidth(), 34);
	}

	private static void dispInfo()
	{
		disp();
		g.drawString(String.format("%6s: Rock", "RED"), 0, 0, 0);
		g.drawString(String.format("%6s: Paper", "GREEN"), 0, 8, 0);
		g.drawString(String.format("%6s: Scissors", "BLUE"), 0, 16, 0);
	}

	private static void dispStats(int player, int computer)
	{
		disp();
		g.drawString(String.format("%6s: Continue", "WHITE"), 0, 0, 0);
		g.drawString(String.format("%6s: Exit", "YELLOW"), 0, 8, 0);
		g.drawString(String.format("You: %s CPU: %s", msg[player], msg[computer]), 0, 36, 0);
		g.drawLine(0, 44, g.getWidth(), 44);
		g.drawString(String.format("You %s", info[computer][player]), g.getWidth() / 2, 46, GraphicsLCD.HCENTER);
	}
}
